package com.hybridtetris.screendimensions;

import android.content.Context;
import android.content.res.Resources;
import android.util.DisplayMetrics;
import android.view.WindowManager;

import com.facebook.react.bridge.ReactApplicationContext;
import com.facebook.react.bridge.ReactContextBaseJavaModule;

import java.util.HashMap;
import java.util.Map;

import javax.annotation.Nullable;

public class ScreenDimensionsModule extends ReactContextBaseJavaModule {
    private static final String MODULE_NAME = "ScreenDimensions";

    private static final String REAL_SCREEN_HEIGHT = "REAL_SCREEN_HEIGHT";
    private static final String REAL_SCREEN_WIDTH = "REAL_SCREEN_WIDTH";
    private static final String NAVIGATION_BAR_HEIGHT = "NAVIGATION_BAR_HEIGHT";

    public ScreenDimensionsModule(ReactApplicationContext reactContext) {
        super(reactContext);
    }

    @Override
    public String getName() {
        return ScreenDimensionsModule.MODULE_NAME;
    }

    @Nullable
    @Override
    public Map<String, Object> getConstants() {
        Map constants = new HashMap<String, Object>();

        constants.put(REAL_SCREEN_HEIGHT, this.getScreenHeight());
        constants.put(REAL_SCREEN_WIDTH, this.getScreenWidth());
        constants.put(NAVIGATION_BAR_HEIGHT, this.getNavBarHeight());

        return constants;
    }

    private float getNavBarHeight() {
        Context context = getReactApplicationContext();
        Resources res = context.getResources();

        int navBarHeightId = res.getIdentifier("navigation_bar_height", "dimen", "android");
        return (float) res.getDimensionPixelSize(navBarHeightId) / this.getMetrics().density;
    }

    private float getScreenHeight() {
        DisplayMetrics metrics = this.getMetrics();
        return metrics.heightPixels / metrics.density;
    }

    private float getScreenWidth() {
        DisplayMetrics metrics = this.getMetrics();
        return metrics.widthPixels / metrics.density;
    }

    private DisplayMetrics getMetrics() {
        DisplayMetrics metrics = new DisplayMetrics();
        WindowManager wMan = (WindowManager)
                this.getReactApplicationContext().getSystemService(Context.WINDOW_SERVICE);
        wMan.getDefaultDisplay().getMetrics(metrics);
        return metrics;
    }

}
