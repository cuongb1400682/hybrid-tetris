import Tetromino from "./Tetromino";

/*
 * represent states for the game
 */
export default class GameState {
    /*
     * the default grid is a 30x15 matrix
     * one unit added to each dimension to represent the border
     */
    static DEFAULT_HEIGHT = 31;
    static DEFAULT_WIDTH = 16;

    /*
     * key code for four directions (left, right, down, up) and rotation
     */
    static KEY_LEFT = 1;
    static KEY_RIGHT = 2;
    static KEY_DOWN = 4;
    static KEY_UP = 8;
    static KEY_ROTATE = 16;

    /*
     * direction of pointer navigation
     */
    static DIRECTIONS = [{x: 0, y: -1}, {x: -1, y: 0}, {x: 0, y: 1}, {x: 1, y: 0}];

    /*
     * index of the four directions (left, up, right, down) in GameState.DIRECTIONS
     */
    static DIRECTION_LEFT = 0;
    static DIRECTION_UP = 1;
    static DIRECTION_RIGHT = 2;
    static DIRECTION_DOWN = 3;

    /*
     * create a new game state instance
     */
    constructor(height = GameState.DEFAULT_HEIGHT,
                width = GameState.DEFAULT_WIDTH) {

        height += 3;
        // each cell grid has the following value:
        //     * is the border
        //     0 is empty cell,
        //     one of the letters [I, J, L, O, S, T, Z] represents the corresponding tetromino
        this._grid = [];
        for (let i = 0; i < height; i++) {
            this._grid.push([]);
            for (let j = 0; j < width; j++)
                this._grid[i].push((i === height - 1 || j === 0 || j === width - 1) ? "*" : 0);
        }

        // height and width of the grid
        this._height = height;
        this._width = width;

        // the point represents the motion of tetromino during the process of changing game state
        this._motionPoint = {x: 0, y: width >>> 1};

        // the adapter products new arbitrary tetrominos
        this._adapter = new Tetromino.Adapter();

        // the current tetromino whose position is changing
        this._currentTetromino = this._adapter.next();

        // event handler emitted if 'this._currentTetromino' has landed on 'the ground'
        this._onContactOccurredHandler = null;

        // event handler emitted if the highest stack height is greater then the grid height
        this._onGameOverHandler = null;

        // event handler emitted if some rows has been reduced
        this._onRowsReducedHandler = null;
    }

    /*
     * get the height of the grid
     */
    get height() {
        return this._height;
    }

    /*
     * get the width of the grid
     */
    get width() {
        return this._width;
    }

    /*
     * get the grid of game state for rendering
     */
    get renderableGrid() {
        return this._grid
            .filter((row, index) => index !== this._height - 1 && index >= 3)
            .map((row) => row.filter((item) => item !== '*'));
    }

    /*
     * get handler function of onContactOccurred event
     */
    get onContactOccurred() {
        return this._onContactOccurredHandler;
    }

    /*
     * set handler function of onContactOccurred event
     */
    set onContactOccurred(value) {
        this._onContactOccurredHandler = value;
    }

    /*
     * get handler function of onGameOver event
     */
    get onGameOver() {
        return this._onGameOverHandler;
    }

    /*
     * set handler function of onGameOver event
     */
    set onGameOver(value) {
        this._onGameOverHandler = value;
    }

    //
    /*
     * get handler function of onGameOver event
     */
    get onRowsReduced() {
        return this._onRowsReducedHandler;
    }

    /*
     * set handler function of onGameOver event
     */
    set onRowsReduced(value) {
        this._onRowsReducedHandler = value;
    }

    /*
     * get the tetromino next to the current one
     */
    get nextTetromino() {
        return this._adapter.peak();
    }

    /*
     * reduce all full rows
     */
    _reduceRow(reducedRows) {
        let emptyRows = [];

        for (let i = 0; i < reducedRows.length; i++) {
            let newRow = new Array(this._width).fill(0);
            newRow[0] = '*';
            newRow[newRow.length - 1] = '*';
            emptyRows.push(newRow);
        }

        this._grid = this._grid.filter(
            (item, index) => reducedRows.indexOf(index) === -1
        );

        this._grid = emptyRows.concat(this._grid);

        if (this._onRowsReducedHandler) {
            this._onRowsReducedHandler(reducedRows);
        }
    }

    /*
     * the hook calls onGameOver and onContactOccurred events
     */
    _onContactOccurred() {
        let currentHeight = 0;
        let reducedRows = [];

        for (let x = 0; x < this._height; x++) {
            let countNonZero = 0;
            for (let y = 0; y < this._width; y++) {
                if (this._grid[x][y] !== 0 && this._grid[x][y] !== '*') {
                    currentHeight = Math.max(currentHeight, this._height - x - 1);
                    if (currentHeight >= this._height - 3) {
                        if (this._onGameOverHandler) {
                            this._onGameOverHandler();
                        }
                        return;
                    }
                    countNonZero++;
                }
            }
            if (countNonZero >= this._width - 2) {
                reducedRows.push(x);
            }
        }

        if (reducedRows.length > 0) {
            this._reduceRow(reducedRows);
        }

        this._currentTetromino = this._adapter.next();
        this._resetMotionPoint();
        if (this._onContactOccurredHandler) {
            this._onContactOccurredHandler();
        }
    }

    /*
     * bring motion point back to its default position
     */
    _resetMotionPoint() {
        this._motionPoint = {x: 0, y: (this.width >>> 1)};
    }

    /*
     * check if the 'point' is inside the 'grid'
     */
    static _isValidCoordinate(point, grid) {
        const height = grid.length;
        const width = grid[0].length;

        const {
            x,
            y,
        } = point;

        return (x >= 0 && x < height) && (y >= 0 && y < width);
    }

    /*
     * engrave the same tetromino into the copy of gameStateGrid at motionPoint
     *
     * motionPoint      represents the current position of tetromino where the tetromino is drawn
     * gameStateGrid    the state containing motionPoint
     * centralPoint     the central point of tetromino
     * visited          marks positions motionPoint has passed
     * fillChar         character to fill in the copy of gameStateGrid
     * newGameStateGrid the copy of gameStateGrid
     */
    _floodFill(motionPoint,
               gameStateGrid,
               centralPoint,
               tetrominoGrid,
               visited,
               fillChar = null,
               newGameStateGrid = null) {

        // the central point has been visited before
        if (visited[centralPoint.x][centralPoint.y])
            return 0;

        // memorize the new visited point
        visited[centralPoint.x][centralPoint.y] = 1;

        // draw new game state
        if (newGameStateGrid) {
            newGameStateGrid[motionPoint.x][motionPoint.y] = fillChar;
        }

        // how many point have we drawn so far?
        let pointCount = 1;

        // for each direction East, West, South, North
        for (let direction of GameState.DIRECTIONS) {
            // move motion point along direction
            let nextMotionPoint = {
                x: motionPoint.x + direction.x,
                y: motionPoint.y + direction.y,
            };

            // move central point along direction
            let nextCentralPoint = {
                x: centralPoint.x + direction.x,
                y: centralPoint.y + direction.y,
            };

            // in case, the points has not reach the border of grids
            if (GameState._isValidCoordinate(nextMotionPoint, gameStateGrid) &&
                GameState._isValidCoordinate(nextCentralPoint, tetrominoGrid)) {
                if (tetrominoGrid[nextCentralPoint.x][nextCentralPoint.y] === 1) {
                    pointCount += this._floodFill(
                        nextMotionPoint,
                        gameStateGrid,
                        nextCentralPoint,
                        tetrominoGrid,
                        visited,
                        fillChar,
                        newGameStateGrid
                    );
                }
            }
        }

        return pointCount;
    }

    /*
     * count the number of cell the 'tetrominoGrid' overlaps
     *
     * motionPoint      represents the current position of tetromino where the tetromino is drawn
     * gameStateGrid    the state containing motionPoint
     * centralPoint     the central point of tetromino
     * visited          marks positions motionPoint has passed
     */
    _countColoredCells(motionPoint,
                       gameStateGrid,
                       centralPoint,
                       tetrominoGrid,
                       visited) {

        // the central point has been visited before
        if (visited[centralPoint.x][centralPoint.y])
            return 0;

        // memorize the new visited point
        visited[centralPoint.x][centralPoint.y] = 1;

        // how many point have we drawn so far?
        let pointCount = 0;
        if (tetrominoGrid[centralPoint.x][centralPoint.y] !== 0) {
            pointCount = Number(gameStateGrid[motionPoint.x][motionPoint.y] !== 0).valueOf();
        }

        // for each direction East, West, South, North
        for (let direction of GameState.DIRECTIONS) {
            // move motion point along direction
            let nextMotionPoint = {
                x: motionPoint.x + direction.x,
                y: motionPoint.y + direction.y,
            };

            // move central point along direction
            let nextCentralPoint = {
                x: centralPoint.x + direction.x,
                y: centralPoint.y + direction.y,
            };

            // in case, the points has not reach the border of grids
            if (GameState._isValidCoordinate(nextMotionPoint, gameStateGrid) &&
                GameState._isValidCoordinate(nextCentralPoint, tetrominoGrid)) {
                pointCount += this._countColoredCells(
                    nextMotionPoint,
                    gameStateGrid,
                    nextCentralPoint,
                    tetrominoGrid,
                    visited
                );
            }
        }

        return pointCount;
    }

    /*
     * check if the tetromino at motionPoint is blocked by any other previous tetrominos
     */
    _isBlocked(motionPoint) {
        const gameStateGrid = this._grid;
        const tetrominoGrid = this._currentTetromino.grid;
        const centralPoint = this._currentTetromino.center;

        let visited = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];

        let count = this._countColoredCells(
            motionPoint, gameStateGrid,
            centralPoint, tetrominoGrid,
            visited
        );

        return count > 0;
    }

    /*
     * draw the tetromino at motionPoint with fillChar
     */
    _drawFigure(motionPoint, fillChar) {
        const gameStateGrid = this._grid;
        const tetrominoGrid = this._currentTetromino.grid;
        const centralPoint = this._currentTetromino.center;

        let visited = [[0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0], [0, 0, 0, 0]];
        let newGameStateGrid = JSON.parse(JSON.stringify(gameStateGrid));

        this._floodFill(
            motionPoint, gameStateGrid,
            centralPoint, tetrominoGrid,
            visited,
            fillChar, newGameStateGrid
        );

        this._grid = newGameStateGrid;
    }

    /*
     * move motion point following direction
     */
    move(direction) {
        const oldMotionPoint = this._motionPoint;
        let newMotionPoint = {
            x: this._motionPoint.x + GameState.DIRECTIONS[direction].x,
            y: this._motionPoint.y + GameState.DIRECTIONS[direction].y,
        };

        let fillChar = this._currentTetromino.constructor.name;

        this._drawFigure(oldMotionPoint, 0);
        if (this._isBlocked(newMotionPoint)) {
            this._drawFigure(oldMotionPoint, fillChar);
            if (direction === GameState.DIRECTION_DOWN)
                this._onContactOccurred();
            return false;
        } else {
            this._drawFigure(newMotionPoint, fillChar);
            this._motionPoint = newMotionPoint;
            return true;
        }
    }

    /*
     * rotate the current tetromino
     */
    rotate() {
        let fillChar = this._currentTetromino.constructor.name;

        let motionPoint = this._motionPoint;

        this._drawFigure(motionPoint, 0);

        this._currentTetromino.rotateLeft();

        if (this._isBlocked(motionPoint)) {
            this._currentTetromino.rotateRight();
        }

        this._drawFigure(motionPoint, fillChar);
    }

    /*
     * move motion point according to pressed keys
     */
    next(key = GameState.KEY_DOWN) {
        if (key & GameState.KEY_LEFT) {
            this.move(GameState.DIRECTION_LEFT);
        }

        if (key & GameState.KEY_RIGHT) {
            this.move(GameState.DIRECTION_RIGHT);
        }

        if (key & GameState.KEY_DOWN) {
            this.move(GameState.DIRECTION_DOWN);
        }

        if (key & GameState.KEY_UP) {
            this.move(GameState.DIRECTION_UP);
        }

        if (key & GameState.KEY_ROTATE) {
            this.rotate();
        }
    }

    /*
     * for debugging
     */
    dataDump() {
        console.log("this._height          : " + this._height);
        console.log("this._width           : " + this._width);
        console.log("this._motionPoint     : " + JSON.stringify(this._motionPoint));
        console.log("this._currentTetromino: " + JSON.stringify(this._currentTetromino));
        for (let i = 0; i < this.height; i++) {
            let line = [];
            for (let j = 0; j < this.width; j++) {
                line.push(` ${this._grid[i][j] === 0 ? '.' : this._grid[i][j]}`);
            }
            console.log(line.join(''));
        }
    }
}

