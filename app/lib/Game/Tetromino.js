/*
 * base class for all other tetrominos
 */
export default class Tetromino {
    /*
     * create a new tetromino with 'white' as default color
     */
    constructor(defaultShape) {
        if (!defaultShape) {
            throw new TypeError("defaultShape must not be null or undefined");
        }
        this._grid = defaultShape;
        this._centralPoints = [
            {x: 1, y: 1},
            {x: 1, y: 1},
            {x: 1, y: 1},
            {x: 1, y: 1},
        ];
        this._centralPointIndex = 3;
    }

    /*
     * get central point of tetromino
     */
    get center() {
        return this._centralPoints[this._centralPointIndex];
    }

    /*
     * get size of edge of grid
     */
    get gridSize() {
        return this._grid.length;
    }

    /*
     * get the matrix of tetromino
     */
    get grid() {
        return this._grid;
    }

    /*
     * get the matrix of tetromino for rendering
     */
    get renderableGrid() {
        const letter = this.constructor.name;
        return this._grid.map((row) => row.map((cell) => cell === 1 ? letter : 0));
    }


    /*
     * make a new empty grid with edge length of this.gridSize
     */
    _generateEmptyGrid() {
        let emptyGrid = [];
        for (let i = 0 ; i < this.gridSize; i++) {
            emptyGrid.push([]);
            for (let j = 0 ; j < this.gridSize; j++)
                emptyGrid[i].push(0);
        }
        return emptyGrid;
    }

    /*
     * transform the matrix by rotating counter clock-wise
     */
    rotateLeft() {
        if (this instanceof Tetromino.VARIANTS.O)
            return;
        let newGrid = this._generateEmptyGrid();
        for (let i = 0; i < this.gridSize; i++)
            for (let j = 0; j < this.gridSize; j++)
                newGrid[this.gridSize - j - 1][i] = this.grid[i][j];
        this._grid = newGrid;
        this._centralPointIndex = (this._centralPointIndex + 3) % 4;
    }

    /*
     * transform the matrix by rotating clock-wise
     */
    rotateRight() {
        if (this instanceof Tetromino.VARIANTS.O)
            return;
        let newGrid = this._generateEmptyGrid();
        for (let i = 0; i < this.gridSize; i++)
            for (let j = 0; j < this.gridSize; j++)
                newGrid[j][this.gridSize - 1 - i] = this.grid[i][j];
        this._grid = newGrid;
        this._centralPointIndex = (this._centralPointIndex + 1) % 4;
    }

    /*
     * for debugging
     */
    dataDump() {
        for (let i = 0; i < this.gridSize; i++) {
            let line = [];
            for (let j = 0; j < this.gridSize; j++) {
                if (i === this.center.x && j === this.center.y)
                    line.push(' X ');
                else
                    line.push(` ${this.grid[i][j] === 1 ? '*' : '.'} `);
            }
            console.log(`[${line.join("")}]`);
        }
        console.log('\n');
    }

    /*
     * set of seven kinds of tetrominos
     */
    static VARIANTS_COUNT = 7;
    static VARIANTS = {
        I: class extends Tetromino {
            constructor() {
                let grid = [[0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0], [0, 1, 0, 0]];
                super(grid);
                this._centralPoints = [
                    {x: 1, y: 1},
                    {x: 1, y: 2},
                    {x: 2, y: 1},
                    {x: 1, y: 1},
                ];
            }
        },
        J: class extends Tetromino {
            constructor() {
                let grid = [[0, 0, 1], [0, 0, 1], [0, 1, 1]];
                super(grid);
                this._centralPoints = [
                    {x: 2, y: 0},
                    {x: 0, y: 0},
                    {x: 0, y: 2},
                    {x: 2, y: 2},
                ];
            }
        },
        L: class extends Tetromino {
            constructor() {
                let grid = [[1, 0, 0], [1, 0, 0], [1, 1, 0]];
                super(grid);
                this._centralPoints = [
                    {x: 0, y: 0},
                    {x: 0, y: 2},
                    {x: 2, y: 2},
                    {x: 2, y: 0},
                ];
            }
        },
        O: class extends Tetromino {
            constructor() {
                let grid = [[1, 1, 0], [1, 1, 0], [0, 0, 0]];
                super(grid);
            }
        },
        S: class extends Tetromino {
            constructor() {
                let grid = [[0, 1, 1], [1, 1, 0], [0, 0, 0]];
                super(grid);
            }
        },
        T: class extends Tetromino {
            constructor() {
                let grid = [[1, 1, 1], [0, 1, 0], [0, 0, 0]];
                super(grid);
                this._centralPoints = [
                    {x: 1, y: 2},
                    {x: 2, y: 1},
                    {x: 1, y: 0},
                    {x: 0, y: 1},
                ];
            }
        },
        Z: class extends Tetromino {
            constructor() {
                let grid = [[1, 1, 0], [0, 1, 1], [0, 0, 0]];
                super(grid);
            }
        },
    };

    /*
     * adapter for generating tetromino, responsible for providing new random tetromino
     */
    static Adapter = class {
        static STACK_SIZE = 8;

        /*
         * create a new adapter
         */
        constructor(adapterCapacity = Tetromino.Adapter.STACK_SIZE) {
            this._stack = [];
            this._capacity = adapterCapacity;
            this._updateStack();
        }

        /*
         * get current size of adapter
         */
        get size() {
            return this._stack.length;
        }

        /*
         * get maximum sie of adapter
         */
        get capacity() {
            return this._capacity;
        }

        /*
         * return the tetromino that will be "pop" out by Tetromino.Adapter.next()
         */
        peak(index = 0) {
            if (this.size < 1)
                this._updateStack();
            if (index >= this._capacity) {
                throw new RangeError(`index = ${index} is out of bound`);
            }
            return this._stack[this.size - 1 - index];
        }

        /*
         * return the tetromino and get rid of it from the adapter
         */
        next() {
            if (this.size < 1)
                this._updateStack();
            return this._stack.pop();
        }

        /*
         * choose an arbitrary tetromino from Tetromino.VARIANTS
         */
        _getRandomConstructor() {
            let index = Math.round(Math.random() * 100) % Tetromino.VARIANTS_COUNT;
            return Tetromino.VARIANTS[Object.keys(Tetromino.VARIANTS)[index]];
        }

        /*
         * fill up the adapter whenever it runs out of tetromino
         */
        _updateStack() {
            while (this.size < this._capacity) {
                let tetrominoConstructor = this._getRandomConstructor();
                let tetromino = new tetrominoConstructor();

                if (!(tetromino instanceof Tetromino.VARIANTS.O)) {
                    let nRotation = Math.round(Math.random() * 100) % 4;
                    while (nRotation--) {
                        tetromino.rotateLeft();
                    }
                }

                this._stack.push(tetromino);
            }
        }
    };
}

