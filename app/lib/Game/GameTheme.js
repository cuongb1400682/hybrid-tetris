/*
 * store color for all kinds of tetrominos
 */
export default class GameTheme {
    static BACKGROUND = "background";
    static BORDER = "border";

    static PAUSE_BACKGROUND = "pause-background";
    static PAUSE_TEXT_COLOR = "pause-text-color";
    static PAUSE_BUTTON_BACKGROUND = "pause-button-background";
    static PAUSE_BUTTON_BORDER_COLOR = "pause-button-border-color";
    static PAUSE_BUTTON_TEXT_COLOR = "pause-button-text-color";

    static GAME_OVER_BACKGROUND = "game-over-background";
    static GAME_OVER_TEXT_COLOR = "game-over-text-color";
    static GAME_OVER_BUTTON_BACKGROUND = "game-over-button-background";
    static GAME_OVER_BUTTON_TEXT_COLOR = "game-over-button-text-color";
    static GAME_OVER_BUTTON_BORDER_COLOR = "game-over-button-border-color";
    static GAME_OVER_PLACEHOLDER_COLOR = "game-over-placeholder-color";

    static TIMEOUT_BACKGROUND = "timeout-background";
    static TIMEOUT_TEXT_COLOR = "timeout-text-color";
    static TIMEOUT_BUTTON_BACKGROUND = "timeout-button-background";
    static TIMEOUT_BUTTON_TEXT_COLOR = "timeout-button-text-color";
    static TIMEOUT_BUTTON_BORDER_COLOR = "timeout-button-border-color";
    static TIMEOUT_PLACEHOLDER_COLOR = "timeout-placeholder-color";

    static MAIN_MENU_PLAY_BUTTON_BORDER_COLOR = "main-menu-play-button-border-color";
    static MAIN_MENU_PLAY_BUTTON_BACKGROUND = "main-menu-play-button-background";
    static MAIN_MENU_PLAY_BUTTON_TEXT_COLOR = "main-menu-play-button-text-color";

    static MAIN_MENU_HIGH_SCORE_BUTTON_BORDER_COLOR = "main-menu-high-score-button-border-color";
    static MAIN_MENU_HIGH_SCORE_BUTTON_BACKGROUND = "main-menu-high-score-button-background";
    static MAIN_MENU_HIGH_SCORE_BUTTON_TEXT_COLOR = "main-menu-high-score-button-text-color";

    static MAIN_MENU_SETTINGS_BUTTON_BORDER_COLOR = "main-menu-settings-button-border-color";
    static MAIN_MENU_SETTINGS_BUTTON_BACKGROUND = "main-menu-settings-button-background";
    static MAIN_MENU_SETTINGS_BUTTON_TEXT_COLOR = "main-menu-settings-button-text-color";

    static MAIN_MENU_HELP_BUTTON_BORDER_COLOR = "main-menu-help-button-border-color";
    static MAIN_MENU_HELP_BUTTON_BACKGROUND = "main-menu-help-button-background";
    static MAIN_MENU_HELP_BUTTON_TEXT_COLOR = "main-menu-help-button-text-color";

    static MAIN_MENU_QUIT_BUTTON_BORDER_COLOR = "main-menu-quit-button-border-color";
    static MAIN_MENU_QUIT_BUTTON_BACKGROUND = "main-menu-quit-button-background";
    static MAIN_MENU_QUIT_BUTTON_TEXT_COLOR = "main-menu-quit-button-text-color";

    /*
     * create new game theme
     */
    constructor(json) {
        if (json instanceof String) {
            json = JSON.parse(json);
        }
        this._json = json;
    }

    /*
     * return a clone of 'this' with the modified property
     */
    modify(key, value) {
        let clone = JSON.parse(JSON.stringify(this._json));
        clone[key] = value;
        return new GameTheme(clone);
    }

    static parse(json) {
        return new GameTheme(json);
    }

    /*
     * specify color for particular tetromino
     * argument 'tetromino' can be string, constructor or instance of Tetromino class
     */
    colorOf(tetromino) {
        let constructorName;

        if (typeof(tetromino) === 'string') {
            constructorName = tetromino;
        } else if (tetromino instanceof Function) {
            constructorName = tetromino.name;
        } else {
            constructorName = tetromino.constructor.name;
        }

        if (!(constructorName in this._json)) {
            throw new Error(`'${constructorName}' is not a valid theme entry`);
        } else {
            return this._json[constructorName];
        }
    }

    /*
     * load supported theme specified by file name
     */
    static loadTheme(fileName) {
        // TODO: load theme content from json file
        return {
            "default": GameTheme.parse({
                "I": "#fc0000",
                "J": "#0000fb",
                "L": "#fca400",
                "O": "#fbfb00",
                "S": "#fb00fb",
                "T": "#00fdfd",
                "Z": "#7ffc00",
                "background": "#000000",
                "border": "#b3b3b3",

                "pause-background": "#2a072a",
                "pause-text-color": "#ffffff",
                "pause-button-background": "#9f4794",
                "pause-button-border-color": "#331041",
                "pause-button-text-color": "#c3c3c3",

                "game-over-background": "#ca000d",
                "game-over-text-color": "#ffffff",
                "game-over-button-background": "#680309",
                "game-over-button-border-color": "#600a16",
                "game-over-button-text-color": "#ffffff",
                "game-over-placeholder-color": "#ffffff",

                "timeout-background": "#020948",
                "timeout-text-color": "#ffffff",
                "timeout-button-background": "#09267c",
                "timeout-button-text-color": "#ffffff",
                "timeout-button-border-color": "#030b5a",
                "timeout-placeholder-color": "#ffffff",

                "main-menu-play-button-border-color": "#006b07",
                "main-menu-play-button-background": "#37c95b",
                "main-menu-play-button-text-color": "#004b0e",

                "main-menu-high-score-button-border-color": "#9e0d18",
                "main-menu-high-score-button-background": "#c90521",
                "main-menu-high-score-button-text-color": "#4b0001",

                "main-menu-settings-button-border-color": "#9e9900",
                "main-menu-settings-button-background": "#c9c700",
                "main-menu-settings-button-text-color": "#4b4900",

                "main-menu-help-button-border-color": "#06039e",
                "main-menu-help-button-background": "#0048c9",
                "main-menu-help-button-text-color": "#0d004b",

                "main-menu-quit-button-border-color": "#009a9e",
                "main-menu-quit-button-background": "#00b3c9",
                "main-menu-quit-button-text-color": "#00384b",
            })
        };
    }

    /*
     * map of all supported config
     */
    static SUPPORTED_THEMES = {
        ...GameTheme.loadTheme("default.json"),
    };
}
