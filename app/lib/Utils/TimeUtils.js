
function standardizeTime(tickCount) {
    let minute = Math.round(tickCount / 60);
    let second = Math.round(tickCount % 60);
    minute = (minute < 10 ? "0" : "").concat(`${minute}`);
    second = (second < 10 ? "0" : "").concat(`${second}`);
    return minute + ":" + second;
}

export {
    standardizeTime,
}
