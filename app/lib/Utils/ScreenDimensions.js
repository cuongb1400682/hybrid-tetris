import {NativeModules, Platform} from "react-native";

let _tmpScreenDim = null;


if (Platform.OS === 'android') { // on the Android Platform
    _tmpScreenDim = NativeModules.ScreenDimensions;
} else { // on the iOS Platform
    const Dimensions = require('react-native').Dimensions;
    const {height, width} = Dimensions.get('window');
    _tmpScreenDim.REAL_SCREEN_HEIGHT = height;
    _tmpScreenDim.REAL_SCREEN_WIDTH = width;
    // TODO: calculate navigation bar height for iOS
    _tmpScreenDim.NAVIGATION_BAR_HEIGHT = 0;
}

_tmpScreenDim.styles = {
    "fullScreen": {
        position: "absolute",
        top: 0,
        left: 0,
        height: _tmpScreenDim.REAL_SCREEN_HEIGHT - _tmpScreenDim.NAVIGATION_BAR_HEIGHT / 2,
        width: _tmpScreenDim.REAL_SCREEN_WIDTH,
    },
};

const ScreenDimensions = _tmpScreenDim;

export {
    ScreenDimensions,
};

