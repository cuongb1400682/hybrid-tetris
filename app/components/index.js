import HighScoreScreen from "./Screens/HighScoreScreen";
import MainMenuScreen from "./Screens/MainMenuScreen";
import PlaygroundScreen from "./Screens/PlaygroundScreen";
import HelpScreen from "./Screens/HelpScreen";
import SettingsScreen from "./Screens/SettingsScreen";

export {
    HighScoreScreen,
    MainMenuScreen,
    PlaygroundScreen,
    HelpScreen,
    SettingsScreen,
};
