import React, {Component} from "react";
import {View, StyleSheet} from "react-native";
import {GameTheme, Tetromino} from "../../lib";

export default class Row extends Component {
    static DEFAULT_SIZE = 20;
    static PADDING_SIZE = 1;

    constructor(props) {
        super(props);

        this.renderCell = this.renderCell.bind(this);

        const {
            size,
            borderSize,
            items,
            theme,
        } = props;

        this.state = {
            size: size,
            borderSize: borderSize,
            theme: theme,
            items: items,
        };
    }

    componentWillReceiveProps(nextProps) {
        if ("items" in nextProps) {
            this.setState({items: nextProps.items});
        }
    }

    renderCell(index, size, borderSize, hexCode) {
        const surplusCellStyle = {
            height: size,
            width: size,
            backgroundColor: hexCode,
            marginRight: borderSize,
        };
        return (
            <View key={index} style={[surplusCellStyle, styles.cell]}/>
        );
    }

    render() {
        //console.log("Row.render");
        const {
            size,
            borderSize,
            theme,
            items,
        } = this.state;

        const cellSize = size || Row.DEFAULT_SIZE;

        const rowWidth = cellSize * items.length;
        const rowHeight = cellSize;

        const surplusRowContainerStyle = {
            width: rowWidth + borderSize * (items.length + 1),
            height: rowHeight + borderSize,
            paddingLeft: borderSize,
            backgroundColor: theme.colorOf(GameTheme.BORDER),
        };

        const rowMap = items.map((item, index) => {
            if (item === 0 || item === '*') {
                return this.renderCell(index, cellSize, borderSize, theme.colorOf(GameTheme.BACKGROUND));
            } else if (item in Tetromino.VARIANTS) {
                return this.renderCell(index, cellSize, borderSize, theme.colorOf(item));
            }
        });

        return (
            <View style={[styles.rowContainer, surplusRowContainerStyle]}>
                {rowMap}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    rowContainer: {
        flexDirection: "row",
        alignItems: "center",
    },
    cell: {},
});

Row.propTypes = {
    items: React.PropTypes.array.isRequired,
    theme: React.PropTypes.instanceOf(GameTheme).isRequired,
    size: React.PropTypes.number,
    borderSize: React.PropTypes.number,
};
