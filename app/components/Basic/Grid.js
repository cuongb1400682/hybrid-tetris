import React, {Component} from "react";
import {View, StyleSheet} from "react-native";
import {GameTheme} from "../../lib";
import Row from "./Row";

export default class Grid extends Component {
    constructor(props) {
        super(props);
        const {
            cellSize,
            borderSize,
            theme,
            gameState,
        } = props;

        const height = gameState.length;
        const width = gameState[0].length;

        this.state = {
            cellSize: cellSize || Row.DEFAULT_SIZE,
            borderSize: borderSize || Row.PADDING_SIZE,
            width: width,
            height: height,
            theme: theme,
            gameState: gameState,
        };
    }

    componentWillReceiveProps(nextProps) {
        if ("gameState" in nextProps) {
            this.setState({
                gameState: nextProps.gameState,
            });
        }
    }

    render() {
        const {
            cellSize,
            borderSize,
            width,
            height,
            gameState,
            theme,
        } = this.state;

        const surplusContainerStyle = {
            height: height * cellSize + borderSize * (height + 2),
            width: width * cellSize + borderSize * (width + 1),
            backgroundColor: theme.colorOf(GameTheme.BORDER),
        };

        return (
            <View style={[styles.container, surplusContainerStyle]}>
                {
                    gameState.map((item, index) => <Row key={index}
                                                        items={item}
                                                        theme={theme}
                                                        size={cellSize}
                                                        borderSize={borderSize}/>)
                }
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
});

Grid.propTypes = {
    cellSize: React.PropTypes.number,
    borderSize: React.PropTypes.number,
    theme: React.PropTypes.instanceOf(GameTheme).isRequired,
    gameState: React.PropTypes.array.isRequired,
};

