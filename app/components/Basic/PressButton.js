import React, {Component} from "react";
import {View, Text, TouchableOpacity, StyleSheet} from "react-native";

export default class PressButton extends Component {
    render() {
        let {
            onPress,
            style,
        } = this.props;

        let moreContainerStyle;
        let moreTextStyle;

        if (style instanceof Array) {
            let _tStyle = {};
            for (let innerStyle of style) {
                _tStyle = {..._tStyle, ...innerStyle};
            }
            style = _tStyle;
        }

        if (style) {
            let {
                color,
                width,
                height,
                alignSelf,
                backgroundColor,
                borderColor,
                margin,
                marginLeft,
                marginRight,
                marginTop,
                marginBottom,
                fontSize,
            } = style;

            if (margin) {
                marginLeft = margin;
                marginRight = margin;
                marginTop = margin;
                marginBottom = margin;
            }

            moreContainerStyle = {
                width: width || styles.container.width,
                height: height || styles.container.height,
                alignSelf: alignSelf || 'auto',
                backgroundColor: backgroundColor || styles.container.backgroundColor,
                borderColor: borderColor || styles.container.borderColor,
                marginLeft: marginLeft || styles.container.marginLeft,
                marginRight: marginRight || styles.container.marginRight,
                marginTop: marginTop || styles.container.marginTop,
                marginBottom: marginBottom || styles.container.marginBottom,
            };


            moreTextStyle = {
                color: color || styles.text.color,
                fontSize: fontSize || styles.text.fontSize,
            };
        }

        return (
            <TouchableOpacity style={[styles.container, moreContainerStyle]}
                              onPress={onPress}>
                <Text style={[styles.text, moreTextStyle]}>
                    {this.props.children.toUpperCase()}
                </Text>
            </TouchableOpacity>
        );
    }
}

const styles = {
    container: {
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#a4a4a4",
        borderWidth: 3,
        borderRadius: 20,
        borderColor: "#686965",
        margin: 0,
        marginLeft: 0,
        marginRight: 0,
        marginTop: 0,
        marginBottom: 0,
        paddingLeft: 8,
        paddingRight: 8,
        paddingTop: 2,
        paddingBottom: 2,
        height: 30,
        opacity: 0.8,
    },
    text: {
        color: "#ffffff",
        fontWeight: "bold",
        fontSize: 12,
        fontFamily: 'verdana',
        opacity: 1.0,
    },
};

PressButton.propTypes = {
    onPress: React.PropTypes.func,
    style: React.PropTypes.oneOf([
        React.PropTypes.object,
        React.PropTypes.arrayOf(React.PropTypes.object),
    ]),
};
