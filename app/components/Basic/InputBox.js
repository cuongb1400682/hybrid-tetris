import React, {Component} from "react";
import {View, Text, TextInput} from "react-native";
import PressButton from "./PressButton";

export default class InputBox extends Component {
    constructor(props) {
        super(props);
        this._onChangeText = this._onChangeText.bind(this);
        this._onSubmitEditing = this._onSubmitEditing.bind(this);
        this._onSubmitPress = this._onSubmitPress.bind(this);
        this.state = {
            text: '',
        };
    }

    render() {
        const {
            buttonStyle,
            placeholderTextColor,
        } = this.props;

        console.log(`buttonStyle = ${JSON.stringify(buttonStyle)}`);

        return (
            <View style={styles.container}>
                <TextInput onChangeText={this._onChangeText}
                           onSubmitEditing={this._onSubmitEditing}
                           style={styles.textInput}
                           placeholderTextColor={placeholderTextColor || styles.textInput.color}
                           placeholder="Your name here!">
                    {this.state.text}
                </TextInput>
                <PressButton onPress={this._onSubmitPress}
                             style={buttonStyle}>Submit</PressButton>
            </View>
        );
    }

    _onChangeText(text) {
        const {
            onChangeText,
        } = this.props;

        let upperText = text.toUpperCase();

        this.setState({text: upperText});

        if (onChangeText) {
            onChangeText(upperText);
        }
    }

    _onSubmitEditing(text) {
        const {
            onSubmitEditing,
        } = this.props;

        if (onSubmitEditing) {
            onSubmitEditing(text);
        }
    }

    _onSubmitPress() {
        const {
            text,
        } = this.state;

        const {
            onSubmit,
        } = this.props;

        if (onSubmit) {
            onSubmit(text);
        }
    }
}

const styles = {
    container: {
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        height: 40,
        width: 300,
    },
    textInput: {
        width: 200,
        marginRight: 16,
        color: "#ffffff",
    },
};

InputBox.propTypes = {
    onChangeText: React.PropTypes.func,
    onSubmitEditing: React.PropTypes.func,
    onSubmit: React.PropTypes.func,
    buttonStyle: React.PropTypes.object,
    placeholderTextColor: React.PropTypes.string,
};
