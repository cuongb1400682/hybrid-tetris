import React, {Component} from "react";
import {View, Text, PanResponder, TouchableWithoutFeedback} from "react-native";
import {Vector, GameState, GameTheme, standardizeTime, ScreenDimensions} from "../../lib";
import Grid from "../Basic/Grid";
import TimeoutScreen from "./TimeoutScreen";
import GameOverScreen from "./GameOverScreen";
import PauseScreen from "./PauseScreen";

export default class PlaygroundScreen extends Component {
    static DEFAULT_CELL_SIZE = 20;
    static DEFAULT_PREVIEW_CELL_SIZE = 16;
    static CHANGE_STATE_INTERVAL = 700;
    static DEFAULT_BORDER_SIZE = 1;
    static TIMEOUT_THRESHOLD = 99 * 60 + 99;

    constructor(props) {
        super(props);

        const {
            onRowsReduced,
            theme,
            score,
        } = props;

        // this.gridWidth = Math.round(width / PlaygroundScreen.DEFAULT_CELL_SIZE) - 4;
        // this.gridHeight = Math.round(height / PlaygroundScreen.DEFAULT_CELL_SIZE) - 4;

        this.gridWidth = 10;
        this.gridHeight = 10;

        this.globalCellSize = PlaygroundScreen.DEFAULT_CELL_SIZE;

        this.gameState = new GameState(this.gridHeight, this.gridWidth);
        this.gameState.onGameOver = this.onGameOver.bind(this);
        this.gameState.onRowsReduced = onRowsReduced || null;

        this.state = {
            grid: this.gameState.renderableGrid,
            nextTetromino: this.gameState.nextTetromino.renderableGrid,
            isPaused: true,
            isGameOver: false,
            timeCount: 0,
            theme: theme,
            score: score || 0,
        };

        this.onTimerChangeStateTick = this.onTimerChangeStateTick.bind(this);
        this.onTimerTimeCountTick = this.onTimerTimeCountTick.bind(this);
        this.onGestureDetectorPress = this.onGestureDetectorPress.bind(this);
        this.onShouldSetPanResponder = this.onShouldSetPanResponder.bind(this);
        this.onGoToHighScoreScreen = this.onGoToHighScoreScreen.bind(this);
        this.onGoToMainMenuScreen = this.onGoToMainMenuScreen.bind(this);
        this.onNameSubmit = this.onNameSubmit.bind(this);
        this.startGame = this.startGame.bind(this);
        this.pauseGame = this.pauseGame.bind(this);
    }

    onGoToHighScoreScreen() {
        const {
            onGoToHighScoreScreen,
        } = this.props;

        if (onGoToHighScoreScreen) {
            onGoToHighScoreScreen();
        }
    }

    onGoToMainMenuScreen() {
        const {
            onGoToMainMenuScreen,
        } = this.props;

        if (onGoToMainMenuScreen) {
            onGoToMainMenuScreen();
        }
    }

    onNameSubmit(name) {
        const {
            onNameSubmit,
        } = this.props;

        const {
            score,
            timeCount,
        } = this.state;

        if (onNameSubmit) {
            onNameSubmit(name, score, timeCount);
        }
    }

    onGameOver() {
        const {
            score,
            timeCount,
        } = this.state;

        const {
            onGameOver,
        } = this.props;

        this.setState({
            isGameOver: true,
        });

        this.pauseGame();

        if (onGameOver) {
            onGameOver(score, timeCount);
        }
    }

    componentWillReceiveProps(nextProps) {
        const {
            theme,
            score,
        } = nextProps;

        this.setState({
            theme: theme,
            score: score || 0,
        });
    }

    getCellCoordinate(gesture) {
        return {
            x: Math.round(gesture.moveX / (this.globalCellSize * 4)),
            y: Math.round(gesture.moveY / (this.globalCellSize * 4)),
        }
    };

    isMovable(point1, point2) {
        let cell1 = this.getCellCoordinate(point1);
        let cell2 = this.getCellCoordinate(point2);
        return (cell1.x !== cell2.x) || (cell1.y !== cell2.y);
    }

    isTimeout() {
        const {
            timeCount,
        } = this.state;

        return timeCount >= PlaygroundScreen.TIMEOUT_THRESHOLD;
    }

    onTimerChangeStateTick() {
        if (this.isTimeout()) {
            this.pauseGame();
        }

        this.gameState.next();

        this.setState({
            grid: this.gameState.renderableGrid,
            nextTetromino: this.gameState.nextTetromino.renderableGrid,
        });
    }

    onTimerTimeCountTick() {
        if (this.isTimeout()) {
            this.pauseGame();
        }

        const {
            timeCount,
        } = this.state;

        const {
            onTimeCountIncrease,
        } = this.props;

        this.setState({
            timeCount: timeCount + 1,
        });

        onTimeCountIncrease(timeCount);
    }

    startGame() {
        const {
            isPaused,
            isGameOver,
        } = this.state;

        if (this.isTimeout() || isGameOver) {
            this.pauseGame();
        }

        if (isPaused) {
            this.timerChangeState = setInterval(
                this.onTimerChangeStateTick,
                PlaygroundScreen.CHANGE_STATE_INTERVAL
            );

            this.timerTimeCount = setInterval(
                this.onTimerTimeCountTick,
                1000
            );

            this.setState({
                isPaused: false,
            });
        }
    }

    pauseGame() {
        const {
            isPaused,
            isGameOver,
        } = this.state;

        if (!isPaused) {
            clearInterval(this.timerChangeState);
            clearInterval(this.timerTimeCount);

            this.timerChangeState = null;
            this.timerTimeCount = null;

            if (!this.isTimeout() && !isGameOver) {
                this.setState({
                    isPaused: true,
                });
            }
        }
    }

    detectKeyPress(hAngleInDeg, vAngleInDeg, currentPoint) {
        const {
            isPaused,
        } = this.state;

        let key = 0;

        if (isPaused)
            return 0;

        // KEY_DOWN
        if (hAngleInDeg < -45 && hAngleInDeg > -135) {
            if (this.isMovable(this.previousPoint, currentPoint)) {
                key = GameState.KEY_DOWN;
            }
        }

        // KEY_LEFT
        if (vAngleInDeg > 45 && vAngleInDeg < 135) {
            if (this.isMovable(this.previousPoint, currentPoint)) {
                key = GameState.KEY_LEFT;
            }
        }

        // KEY_RIGHT
        if (vAngleInDeg < -45 && vAngleInDeg > -135) {
            if (this.isMovable(this.previousPoint, currentPoint)) {
                key = GameState.KEY_RIGHT;
            }
        }

        return key;
    }

    onShouldSetPanResponder() {
        const {
            isGameOver,
        } = this.state;

        return !(isGameOver || this.isTimeout());
    }

    onGestureDetectorPress() {
        this.gameState.next(GameState.KEY_ROTATE);

        this.setState({
            grid: this.gameState.renderableGrid,
        });
    }

    componentWillMount() {
        this.panResponder = PanResponder.create({
            onMoveShouldSetPanResponder: this.onShouldSetPanResponder,
            onMoveShouldSetPanResponderCapture: this.onShouldSetPanResponder,
            onStartShouldSetResponder: this.onShouldSetPanResponder,
            onStartShouldSetResponderCapture: this.onShouldSetPanResponder,

            onPanResponderGrant: (e, gesture) => {
                this.previousPoint = {
                    x: gesture.moveX,
                    y: gesture.moveY,
                };

                this.togglePauseGame = false;
            },

            onPanResponderMove: (e, gesture) => {
                const {
                    moveX,
                    moveY,
                    isPaused,
                } = gesture;

                const motionVector = new Vector(
                    this.previousPoint.x - moveX,
                    this.previousPoint.y - moveY,
                );

                const currentPoint = {
                    x: moveX,
                    y: moveY,
                };

                const hAngleInDeg = motionVector.horizontalAngleDeg();
                const vAngleInDeg = motionVector.verticalAngleDeg();

                // KEY_UP
                if (hAngleInDeg > 45 && hAngleInDeg < 135) {
                    this.togglePauseGame = true;
                } else {
                    if (!isPaused) {
                        let key = this.detectKeyPress(hAngleInDeg, vAngleInDeg, currentPoint);
                        this.gameState.next(key);
                        this.setState({
                            grid: this.gameState.renderableGrid,
                        });
                        this.previousPoint = currentPoint;
                    }
                }
            },

            onPanResponderRelease: (e, gesture) => {
                const {
                    isPaused,
                } = this.state;

                if (this.togglePauseGame) {
                    if (isPaused)
                        this.startGame();
                    else
                        this.pauseGame();
                    this.togglePauseGame = false;
                }

            },
        });

    }

    componentDidMount() {
        this.startGame();
    }

    componentWillUnmount() {
        this.pauseGame();
    }

    render() {
        const {
            timeCount,
            theme,
            score,
        } = this.state;

        const previewTheme = theme.modify(
            GameTheme.BORDER,
            theme.colorOf(GameTheme.BACKGROUND)
        );

        const fullScreen = ScreenDimensions.styles.fullScreen;

        return (
            <View {...this.panResponder.panHandlers} style={[styles.container, fullScreen]}>
                <TouchableWithoutFeedback style={styles.gestureTouchableNoFeedback}
                                          onPress={this.onGestureDetectorPress}>
                    <View style={styles.gameScreen}>
                        <View style={styles.gridContainer}>
                            <Grid theme={theme}
                                  gameState={this.state.grid}
                                  cellSize={PlaygroundScreen.DEFAULT_CELL_SIZE}
                                  borderSize={PlaygroundScreen.DEFAULT_BORDER_SIZE}/>
                        </View>
                        <View style={styles.gameStateContainer}>
                            <View style={styles.infoGroup}>
                                <Text style={styles.textLabel}>Next</Text>
                                <Grid theme={previewTheme}
                                      gameState={this.state.nextTetromino}
                                      cellSize={PlaygroundScreen.DEFAULT_PREVIEW_CELL_SIZE}
                                      borderSize={PlaygroundScreen.DEFAULT_BORDER_SIZE}/>
                            </View>
                            <View style={styles.infoGroup}>
                                <Text style={styles.textLabel}>Score</Text>
                                <Text style={styles.textInfo}>{score}</Text>
                            </View>
                            <View style={styles.infoGroup}>
                                <Text style={styles.textLabel}>Time</Text>
                                <Text style={styles.textInfo}>
                                    {standardizeTime(timeCount)}
                                </Text>
                            </View>
                        </View>
                    </View>
                </TouchableWithoutFeedback>
                {this.renderOverlayScreen()}
            </View>
        );
    }

    renderOverlayScreen() {
        const {
            isPaused,
            isGameOver,
            score,
            timeCount,
        } = this.state;

        const {
            theme,
        } = this.props;

        if (isGameOver) {
            return <GameOverScreen theme={theme}
                                   score={score}
                                   tickCount={timeCount}
                                   onHighScorePress={this.onGoToHighScoreScreen}
                                   onMainMenuButtonPress={this.onGoToMainMenuScreen}
                                   onSubmitButtonPress={this.onNameSubmit}/>
        } else if (isPaused) {
            return <PauseScreen theme={theme}
                                onHighScorePress={this.onGoToHighScoreScreen}
                                onMainMenuButtonPress={this.onGoToMainMenuScreen}/>
        } else if (this.isTimeout()) {
            return <TimeoutScreen theme={theme}
                                  score={score}
                                  onHighScorePress={this.onGoToHighScoreScreen}
                                  onMainMenuButtonPress={this.onGoToMainMenuScreen}
                                  onSubmitButtonPress={this.onNameSubmit}/>
        } else {
            return null;
        }
    }
}

const styles = {
    container: {},
    gestureTouchableNoFeedback: {
        flex: 1,
    },
    gameScreen: {
        flex: 1,
        flexDirection: "row",
        backgroundColor: "black",
    },
    gridContainer: {
        justifyContent: "center",
        paddingLeft: 8,
    },
    gameStateContainer: {
        flexDirection: "column",
        flex: 1,
        justifyContent: "space-around",
        alignItems: "center",
    },
    textLabel: {
        color: "white",
    },
    textInfo: {
        fontSize: 18,
        color: "white",
    },
    gestureDetector: {
        top: 0,
        left: 0,
    },
    infoGroup: {
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
    },
};

PlaygroundScreen.propTypes = {
    onGameOver: React.PropTypes.func,
    onRowsReduced: React.PropTypes.func,
    theme: React.PropTypes.instanceOf(GameTheme).isRequired,
    score: React.PropTypes.number,
    onGoToHighScoreScreen: React.PropTypes.func,
    onGoToMainMenuScreen: React.PropTypes.func,
    onNameSubmit: React.PropTypes.func,
    onTimeCountIncrease: React.PropTypes.func,
};

PlaygroundScreen.defaultProps = {
    theme: GameTheme.SUPPORTED_THEMES.default,
};
