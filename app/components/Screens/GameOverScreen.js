import React, {Component} from "react";
import {View, Text, TextInput} from "react-native";
import {GameTheme, standardizeTime, ScreenDimensions} from "../../lib";
import PressButton from "../Basic/PressButton";
import InputBox from "../Basic/InputBox";

export default class GameOverScreen extends Component {
    constructor(props) {
        super(props);
        this.onNameSubmit = this.onNameSubmit.bind(this);
        this.onMainMenuButtonPress = this.onMainMenuButtonPress.bind(this);
        this.onHighScorePress = this.onHighScorePress.bind(this);
    }

    onNameSubmit(userName) {
        const {
            onSubmitButtonPress,
        } = this.props;

        if (onSubmitButtonPress) {
            onSubmitButtonPress(userName);
        }
    }

    onMainMenuButtonPress() {
        const {
            onMainMenuButtonPress,
        } = this.props;

        if (onMainMenuButtonPress) {
            onMainMenuButtonPress();
        }
    }

    onHighScorePress() {
        const {
            onHighScorePress,
        } = this.props;

        if (onHighScorePress) {
            onHighScorePress();
        }
    }

    render() {
        const {
            theme,
            score,
            tickCount,
        } = this.props;

        const containerBackgroundStyle = {
            backgroundColor: theme.colorOf(GameTheme.GAME_OVER_BACKGROUND),
        };

        const fullScreen = ScreenDimensions.styles.fullScreen;

        const buttonBackgroundStyle = {
            backgroundColor: theme.colorOf(GameTheme.GAME_OVER_BUTTON_BACKGROUND),
            borderColor: theme.colorOf(GameTheme.GAME_OVER_BUTTON_BORDER_COLOR),
            color: theme.colorOf(GameTheme.GAME_OVER_BUTTON_TEXT_COLOR),
        };

        const textColor = {
            color: theme.colorOf(GameTheme.GAME_OVER_TEXT_COLOR),
        };

        const placeholderTextColor = theme.colorOf(GameTheme.GAME_OVER_PLACEHOLDER_COLOR);

        return (
            <View style={[styles.container, containerBackgroundStyle, fullScreen]}>
                <View style={styles.upperViewContainer}>
                    <Text style={[styles.textMessage, textColor]}>:( Sorry, You lose!</Text>
                    <Text
                        style={[styles.textInfo, textColor]}>{`You got ${score || 0} in ${standardizeTime(tickCount || 0)}`}</Text>
                </View>
                <View style={styles.lowerViewsContainer}>
                    <View style={{height: 50}}/>
                    <InputBox onSubmitEditing={this.onNameSubmit}
                              onSubmit={this.onNameSubmit}
                              buttonStyle={buttonBackgroundStyle}
                              placeholderTextColor={placeholderTextColor}/>
                    <View style={{flex:1, alignSelf:"stretch", flexDirection: "column"}}>
                        <View style={{flex:1}}/>
                        <View style={styles.buttonContainer}>
                            <PressButton onPress={this.onMainMenuButtonPress}
                                         style={buttonBackgroundStyle}>Main Menu</PressButton>
                            <PressButton onPress={this.onHighScorePress}
                                         style={buttonBackgroundStyle}>High Score</PressButton>
                        </View>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        opacity: 0.7,
        flexDirection: "column",
    },
    upperViewContainer: {
        flexDirection: "column",
        justifyContent: "flex-end",
        alignItems: "center",
        flex: 1,
    },
    lowerViewsContainer: {
        flex: 2,
        flexDirection: "column",
        alignItems: "center",
    },
    textMessage: {
        fontWeight: "bold",
        fontSize: 42,
    },
    textInfo: {
        fontWeight: "bold",
        fontSize: 20,
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "stretch",
        paddingRight: 12,
        paddingLeft: 12,
        paddingBottom: 12,
    },
};

GameOverScreen.propTypes = {
    theme: React.PropTypes.instanceOf(GameTheme),
    score: React.PropTypes.number,
    tickCount: React.PropTypes.number,
    onSubmitButtonPress: React.PropTypes.func,
    onMainMenuButtonPress: React.PropTypes.func,
    onHighScorePress: React.PropTypes.func,
};

GameOverScreen.defaultProps = {
    theme: GameTheme.SUPPORTED_THEMES.default,
    score: 0,
    tickCount: 0,
};
