import React, {Component} from "react";
import {View, Text, ListView, Image, TouchableOpacity} from "react-native";
import {database} from "firebase";
import {standardizeTime} from "../../lib";
import {images} from "../../config/images";

export default class HighScoreScreen extends Component {
    constructor(props) {
        super(props);

        const {
            dsRef,
        } = props;

        dsRef.on('value', () => {
            this.setState({
                hasDataChanged: true,
            });
        });

        this.ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});

        this.state = {
            dataSource: this.ds.cloneWithRows([]),
            hasProblem: false,
            isLoading: false,
            hasDataChanged: false,
        };

        this.renderRow = this.renderRow.bind(this);
        this.onBackButtonPress = this.onBackButtonPress.bind(this);
        this.fetchUserData = this.fetchUserData.bind(this);
    }

    async fetchUserData() {
        const {
            dsRef,
        } = this.props;

        this.setState({
            isLoading: true,
        });

        let userDataList = [];

        return await dsRef
            .orderByChild('scr')
            .once('value')
            .then((snapshot) => {
                snapshot.forEach((item) => {
                    userDataList.push(item.val());
                });

                if (userDataList.length === 0) {
                    throw 'no data';
                }

                this.setState({
                    isLoading: false,
                    hasDataChanged: false,
                    hasProblem: false,
                    dataSource: this.ds.cloneWithRows(userDataList),
                });
            })
            .catch(() => {
                this.setState({
                    isLoading: false,
                    hasDataChanged: true,
                    hasProblem: true,
                })
            });
    }

    async componentDidMount() {
        await this.fetchUserData();
    }

    componentWillUnmount() {
        const {
            dsRef,
        } = this.props;
        dsRef.off();
    }

    renderRow(rowData, sectionID, rowID) {
        let rowBackground = {
            backgroundColor: (rowID & 1) ? "#e4e4e4" : "#ffffff",
        };

        return (
            <View style={[styles.rowContainer, rowBackground]}>
                <Text style={styles.fieldUserName}>{rowData.usr}</Text>
                <Text style={styles.fieldUserScore}>{rowData.scr}</Text>
                <Text style={styles.fieldTimeCount}>{standardizeTime(rowData.tmc)}</Text>
            </View>
        );
    }

    onBackButtonPress() {
        const {
            onBackButtonPress,
        } = this.props;

        if (onBackButtonPress) {
            onBackButtonPress();
        }
    }

    renderHeader() {
        const {
            hasDataChanged,
        } = this.state;

        const rightButton = (
            <TouchableOpacity style={styles.listViewHeaderIconContainer}
                              onPress={this.fetchUserData}>
                <Image style={styles.listViewHeaderRightButtonIcon}
                       source={images.icons.reloadIcon}>
                </Image>
            </TouchableOpacity>
        );

        return (
            <View style={styles.listViewHeader}>
                <TouchableOpacity style={styles.listViewHeaderIconContainer}
                                  onPress={this.onBackButtonPress}>
                    <Image style={styles.listViewHeaderLeftButtonIcon}
                           source={images.icons.backIcon}>
                    </Image>
                </TouchableOpacity>
                <Text style={styles.listViewHeaderText}>
                    TOP LIST
                </Text>
                {hasDataChanged ? rightButton : <View style={styles.listViewHeaderPlaceholder}/>}
            </View>
        )
    }

    renderSeparator(sectionId, rowId) {
        return <View key={rowId} style={styles.separator}/>;
    }

    renderListView() {
        const {
            dataSource,
        } = this.state;

        return (
            <ListView style={styles.listView}
                      enableEmptySections={true}
                      dataSource={dataSource}
                      renderRow={this.renderRow}
                      renderSeparator={this.renderSeparator}/>
        );
    }

    renderProblemMessage() {
        return (
            <Text>
                Some problems has occurred.
                Cannot load the top list.
            </Text>
        )
    }

    renderLoadingMessage() {
        return (
            <Text>
                Please wait for loading data
            </Text>
        );
    }

    render() {
        const {
            hasProblem,
            isLoading,
        } = this.state;

        const centerizeView = {
            justifyContent: "center",
            alignItems: "center",
        };

        let centerView = null;

        if (isLoading)
            centerView = this.renderLoadingMessage();
        else if (hasProblem)
            centerView = this.renderProblemMessage();
        else
            centerView = this.renderListView();

        return (
            <View style={styles.container}>
                {this.renderHeader()}
                <View style={[styles.container, (hasProblem || isLoading) && centerizeView]}>
                    {centerView}
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },
    listViewHeader: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignItems: "center",
        height: 50,
        backgroundColor: "brown",
    },
    listViewHeaderText: {
        fontSize: 30,
        fontWeight: "bold",
        textAlign: "center",
        textAlignVertical: "center",
    },
    listViewHeaderIconContainer: {
        width: 50,
        height: 50,
        justifyContent: "center",
        alignItems: "center",
    },
    listViewHeaderRightButtonIcon: {
        width: 24,
        height: 24,
    },
    listViewHeaderLeftButtonIcon: {
        width: 28,
        height: 28,
    },
    listViewHeaderPlaceholder: {
        width: 50,
        height: 50,
    },
    listView: {
        flex: 1,
    },
    separator: {
        height: 1,
        backgroundColor: "#c4c4c4",
    },
    rowContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        padding: 8,
    },
    fieldUserName: {
        fontWeight: "bold",
        fontSize: 18,
        flex: 1,
    },
    fieldUserScore: {
        fontSize: 18,
        flex: 1,
        textAlign: "center",
    },
    fieldTimeCount: {
        fontSize: 14,
        flex: 1,
        textAlign: "right",
    },
};

HighScoreScreen.propTypes = {
    dsRef: React.PropTypes.instanceOf(database.Reference).isRequired,
    onBackButtonPress: React.PropTypes.func,
};
