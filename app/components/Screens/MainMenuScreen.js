import React, {Component} from "react";
import {View, Text, Image} from "react-native";
import PressButton from "../Basic/PressButton";
import {ScreenDimensions, GameTheme} from "../../lib";
import {images} from "../../config/images";

export default class MainMenuScreen extends Component {
    constructor(props) {
        super(props);
        this.playButtonPress = this.playButtonPress.bind(this);
        this.highScoreButtonPress = this.highScoreButtonPress.bind(this);
        this.settingsButtonPress = this.settingsButtonPress.bind(this);
        this.helpButtonPress = this.helpButtonPress.bind(this);
    }

    playButtonPress() {
        const {
            playButtonPress,
        } = this.props;

        if (playButtonPress) {
            playButtonPress();
        }
    }

    highScoreButtonPress() {
        const {
            highScoreButtonPress,
        } = this.props;

        if (highScoreButtonPress) {
            highScoreButtonPress();
        }
    }

    settingsButtonPress() {
        const {
            settingsButtonPress,
        } = this.props;

        if (settingsButtonPress) {
            settingsButtonPress();
        }
    }

    helpButtonPress() {
        const {
            helpButtonPress,
        } = this.props;

        if (helpButtonPress) {
            helpButtonPress();
        }
    }

    render() {
        let {
            theme,
        } = this.props;

        const fullScreen = ScreenDimensions.styles.fullScreen;

        const playButton = {
            borderColor: theme.colorOf(GameTheme.MAIN_MENU_PLAY_BUTTON_BORDER_COLOR),
            backgroundColor: theme.colorOf(GameTheme.MAIN_MENU_PLAY_BUTTON_BACKGROUND),
            color: theme.colorOf(GameTheme.MAIN_MENU_PLAY_BUTTON_TEXT_COLOR),
        };

        const highScoreButton = {
            borderColor: theme.colorOf(GameTheme.MAIN_MENU_HIGH_SCORE_BUTTON_BORDER_COLOR),
            backgroundColor: theme.colorOf(GameTheme.MAIN_MENU_HIGH_SCORE_BUTTON_BACKGROUND),
            color: theme.colorOf(GameTheme.MAIN_MENU_HIGH_SCORE_BUTTON_TEXT_COLOR),
        };

        const settingsButton = {
            borderColor: theme.colorOf(GameTheme.MAIN_MENU_SETTINGS_BUTTON_BORDER_COLOR),
            backgroundColor: theme.colorOf(GameTheme.MAIN_MENU_SETTINGS_BUTTON_BACKGROUND),
            color: theme.colorOf(GameTheme.MAIN_MENU_SETTINGS_BUTTON_TEXT_COLOR),
        };

        const helpButton = {
            borderColor: theme.colorOf(GameTheme.MAIN_MENU_HELP_BUTTON_BORDER_COLOR),
            backgroundColor: theme.colorOf(GameTheme.MAIN_MENU_HELP_BUTTON_BACKGROUND),
            color: theme.colorOf(GameTheme.MAIN_MENU_HELP_BUTTON_TEXT_COLOR),
        };

        return (
            <View style={styles.container}>
                <Image source={images.mainScreenBackground}
                       style={fullScreen}
                       resizeMode="stretch">
                    <View style={{flex: 1, flexDirection: "column",justifyContent: "center"}}>
                        <PressButton style={[styles.pressButton, playButton]}
                                     onPress={this.playButtonPress}>play</PressButton>
                        <PressButton style={[styles.pressButton, highScoreButton]}
                                     onPress={this.highScoreButtonPress}>high score</PressButton>
                        <PressButton style={[styles.pressButton, settingsButton]}
                                     onPress={this.settingsButtonPress}>settings</PressButton>
                        <PressButton style={[styles.pressButton, helpButton]}
                                     onPress={this.helpButtonPress}>help</PressButton>
                    </View>
                </Image>
            </View>
        );
    }
}

const styles = {
    container: {
        flex: 1,
    },
    pressButton: {
        marginTop: 8,
        marginLeft: 8,
        marginRight: 8,
        height: 44,
        fontSize: 26,
    },
};

MainMenuScreen.propTypes = {
    playButtonPress: React.PropTypes.func,
    highScoreButtonPress: React.PropTypes.func,
    settingsButtonPress: React.PropTypes.func,
    helpButtonPress: React.PropTypes.func,
    theme: React.PropTypes.instanceOf(GameTheme),
};

MainMenuScreen.defaultProps = {
    theme: GameTheme.SUPPORTED_THEMES.default,
};
