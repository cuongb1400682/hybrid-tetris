import React, {Component} from "react";
import {View, Text, TextInput} from "react-native";
import {GameTheme, ScreenDimensions} from "../../lib";
import PressButton from "../Basic/PressButton";

export default class PauseScreen extends Component {
    constructor(props) {
        super(props);
        this.onMainMenuButtonPress = this.onMainMenuButtonPress.bind(this);
        this.onHighScorePress = this.onHighScorePress.bind(this);
    }

    onMainMenuButtonPress() {
        const {
            onMainMenuButtonPress,
        } = this.props;

        if (onMainMenuButtonPress) {
            onMainMenuButtonPress();
        }
    }

    onHighScorePress() {
        const {
            onHighScorePress,
        } = this.props;

        if (onHighScorePress) {
            onHighScorePress();
        }
    }

    render() {
        const {
            theme,
        } = this.props;

        const containerBackgroundStyle = {
            backgroundColor: theme.colorOf(GameTheme.PAUSE_BACKGROUND),
        };

        const fullScreen = ScreenDimensions.styles.fullScreen;

        const buttonBackgroundStyle = {
            backgroundColor: theme.colorOf(GameTheme.PAUSE_BUTTON_BACKGROUND),
            borderColor: theme.colorOf(GameTheme.PAUSE_BUTTON_BORDER_COLOR),
            color: theme.colorOf(GameTheme.PAUSE_BUTTON_TEXT_COLOR),
        };

        const textColor = {
            color: theme.colorOf(GameTheme.PAUSE_TEXT_COLOR),
        };

        return (
            <View style={[styles.container, containerBackgroundStyle, fullScreen]}>
                <View style={styles.textContainer}>
                    <Text style={[styles.textMessage, textColor]}>PAUSED</Text>
                    <Text style={[styles.textInfo, textColor]}>Swipe up to resume the game</Text>
                </View>
                <View style={styles.buttonContainer}>
                    <PressButton onPress={this.onMainMenuButtonPress}
                                 style={buttonBackgroundStyle}>Main Menu</PressButton>
                    <PressButton onPress={this.onHighScorePress}
                                 style={buttonBackgroundStyle}>High Score</PressButton>
                </View>
            </View>
        );
    }
}

const styles = {
    container: {
        opacity: 0.8,
        flexDirection: "column",
    },
    textMessage: {
        fontWeight: "bold",
        fontSize: 84,
    },
    textInfo: {
        fontSize: 16,
    },
    buttonContainer: {
        flexDirection: "row",
        justifyContent: "space-between",
        alignSelf: "stretch",
        paddingRight: 12,
        paddingLeft: 12,
        paddingBottom: 12,
    },
    textContainer: {
        flex: 1,
        flexDirection: "column",
        justifyContent: "center",
        alignItems: "center",
        alignSelf: "stretch",
    },
};

PauseScreen.propTypes = {
    theme: React.PropTypes.instanceOf(GameTheme),
    onMainMenuButtonPress: React.PropTypes.func,
    onHighScorePress: React.PropTypes.func,
};

PauseScreen.defaultProps = {
    theme: GameTheme.SUPPORTED_THEMES.default,
};
