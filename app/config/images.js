const images = {
    mainScreenBackground: require('../images/bkg_main_screen.jpg'),
    icons: {
        backIcon: require('../images/icon_back.png'),
        reloadIcon: require('../images/icon_reload.png'),
    },
};

export {
    images,
}

