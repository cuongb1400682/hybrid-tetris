const routes = {
    mainMenu: {
        title: 'Main Menu',
        index: 0,
    },
    playGround: {
        title: 'Play Ground',
        index: 1,
    },
    help: {
        title: 'Help',
        index: 2,
    },
    settings: {
        title: 'Settings',
        index: 3,
    },
    highScore: {
        title: 'High Score',
        index: 4,
    },
};

export {
    routes
}
