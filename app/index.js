import React, {Component} from "react";
import {View, Text, Navigator, Alert} from "react-native";
import * as firebase from "firebase";
import {PlaygroundScreen, HighScoreScreen, MainMenuScreen, SettingsScreen, HelpScreen} from "./components";
import {GameTheme} from "./lib";
import {routes} from "./config/routes";
import {userAchievementDbConfig} from "./config/firebase";

export default class App extends Component {
    constructor(props) {
        super(props);
        this.state = {
            score: 0,
        };

        firebase.initializeApp(userAchievementDbConfig.config);
        this.userDataRef = firebase.database().ref('userdata');

        this.timeCount = 0;
        this.renderScene = this.renderScene.bind(this);
        this.renderScene = this.renderScene.bind(this);
        this.onIncreaseScore = this.onIncreaseScore.bind(this);
        this.onTimeCountIncrease = this.onTimeCountIncrease.bind(this);
    }

    async writeUserData(userName, userScore, timeCount) {
        return await this.userDataRef.push({
            usr: userName,
            scr: userScore,
            tmc: timeCount,
        });
    }

    onTimeCountIncrease(timeCount) {
        this.timeCount = timeCount;
    }

    onIncreaseScore(reducedRows) {
        const {
            score,
        } = this.state;

        this.setState({
            score: score + 10 * reducedRows.length,
        });
    }


    async onNameSubmit(navigator, userName, score, timeCount) {
        if (!userName) {
            return;
        }
        await this.writeUserData(userName, score, timeCount)
            .then(() => {
                navigator.replace(routes.highScore);
            });
    }

    renderScene(route, navigator) {
        // TODO: load theme from user's configuration
        const theme = GameTheme.SUPPORTED_THEMES.default;

        const {
            score,
        } = this.state;

        switch (route.index) {
            case routes.mainMenu.index:
                return (
                    <MainMenuScreen playButtonPress={() => navigator.push(routes.playGround)}
                                    settingsButtonPress={() => navigator.push(routes.settings)}
                                    highScoreButtonPress={() => navigator.push(routes.highScore)}
                                    helpButtonPress={() => navigator.push(routes.help)}/>
                );
            case routes.highScore.index:
                return (
                    <HighScoreScreen dsRef={this.userDataRef}
                                     onBackButtonPress={() => navigator.pop()}/>
                );
            case routes.playGround.index:
                return (
                    <PlaygroundScreen theme={theme}
                                      score={score}
                                      onRowsReduced={this.onIncreaseScore}
                                      onNameSubmit={(userName, score, timeCount) => this.onNameSubmit(navigator, userName, score, timeCount)}
                                      onGoToHighScoreScreen={() => navigator.push(routes.highScore)}
                                      onTimeCountIncrease={this.onTimeCountIncrease}
                                      onGoToMainMenuScreen={() => {
                                          Alert.alert(
                                              "Confirm",
                                              "Do you really want to go back?\nYour score can be lost!",
                                              [
                                                  {
                                                      text: 'Yes',
                                                      onPress: navigator.pop,
                                                  },
                                                  {
                                                      text: 'No',
                                                      onPress: null,
                                                  },
                                              ]
                                          )
                                      }}/>
                );
            case routes.settings.index:
                return (
                    <SettingsScreen/>
                );
            case routes.help.index:
                return (
                    <HelpScreen/>
                );
            default:
                return null;
        }
    }

    render() {
        return (
            <Navigator
                initialRoute={routes.mainMenu}
                renderScene={this.renderScene}
            />
        );
    }
}
