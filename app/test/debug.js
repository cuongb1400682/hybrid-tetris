function dump(visited) {
    for (let i = 0; i < visited.length; i++) {
        let row = [];
        for (let j = 0; j < visited.length; j++) {
            row.push(` ${visited[i][j] ? '*' : '.'} `);
        }
        console.log(`[${row.join('')}]`);
    }
}

