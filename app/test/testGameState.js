import {GameState} from "../lib";

function testGameState() {
    let x = new GameState(10, 10);
    let row = new Array(x.width).fill('I');
    row[0] = '*';
    row[row.length - 1] = '*';
    x._grid[x.height - 2] = row;
    x._onContactOccurred();
}

export {
    testGameState,
};
